# doc.asqatasun.org

This repository holds the very basic structure of <https://doc.asqatasun.org>

Content is in the `src/` folder.

Please note that the content of `src/v5/` is placed in the repository
<https://gitlab.com/asqatasun/documentation/> and is automatically deployed by its own CI.
